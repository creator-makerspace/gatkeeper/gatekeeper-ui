import {Component} from '@angular/core';
import {Router} from '@angular/router';

import {CredentialSvc} from '../credentials.service';
import {UserSvc} from '../users.service';

@Component({
  selector: 'app-credential-create',
  templateUrl: './credential-create.component.html',
})
export class CredentialCreateComponent {
  credential: any = {};

  availableUsers: Array<any> = [];
  loadingUsers = true;

  constructor(
    private router: Router,
    private creds: CredentialSvc,
    private users: UserSvc
  ) {
    this.refreshUsers();
  }

  onSubmit(value: any) {
    this.creds.create(value).subscribe(
      () => {
        this.router.navigate(['/credentials/list']);
      }
    );
  }

  refreshUsers() {
    this.loadingUsers = true
    this.availableUsers = [];

    this.users.getUsers().subscribe(
      availableUsers => {
        this.loadingUsers = false;

        this.availableUsers = availableUsers.sort((a, b) => {
          if (a.name < b.name) { return -1 }
          if (a.name > b.name) { return 1 }
          return 0;
        })
      }
    );
  }
}
