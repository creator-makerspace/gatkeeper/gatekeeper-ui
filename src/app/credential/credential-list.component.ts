import {Component} from '@angular/core';
import {Observable} from 'rxjs';

import {CredentialSvc} from '../credentials.service';
import {UserSvc} from '../users.service';

@Component({
  selector: 'app-credential-list',
  templateUrl: './credential-list.component.html',
})
export class CredentialListComponent {
  credentials: Array<any>;
  usersMap = {};
  selectedCredentials = [];

  constructor(private credSvc: CredentialSvc, private userSvc: UserSvc) {
    this.refresh();
  }

  refresh() {
    this.credSvc.getCredentials()
      .do(data => this.credentials = data)
      .mergeMap(credentials => Observable.from(credentials))
      .map(c => c['user_id'])
      .distinct()
      .mergeMap(id => this.userSvc.getUser(id))
      .subscribe(user => this.usersMap[user['id']] = user);
  }

  checkedChange($event) {
    if ($event) {
      this.selectedCredentials = [];
    }
  }

  revoke() {
    Observable.from(this.selectedCredentials)
      .flatMap(i => this.credSvc.delete(i))
      .finally(() => this.refresh())
      .subscribe();
  }
}
