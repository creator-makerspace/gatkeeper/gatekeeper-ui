import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';

/* Controller to facilitate reception of the openid redirect */
@Component({
  selector: 'app-openid',
  template: 'Please wait....',
})
export class OpenIDComponent implements OnInit {
  constructor(
    private router: Router,
    private authSvc: OAuthService
  ) {
  }

  ngOnInit() {
    if (window.location.search !== '') {
      this.authSvc.tryLogin().then(() => {
        this.router.navigate(['/'])
      })
    }
  }
}
