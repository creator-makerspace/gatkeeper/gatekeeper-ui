import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {PointsComponent} from './point.component';
import {PointListComponent} from './point-list.component';

import {SharedModule} from '../shared/shared.module';

import {routing} from './point.routing';

@NgModule({
  declarations: [
    PointsComponent,
    PointListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    routing,
  ]
})
export class PointModule {
}
