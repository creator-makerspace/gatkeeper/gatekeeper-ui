import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {APIClient} from '../apiclient.service';
import {PointSvc} from '../points.service';


@Component({
  selector: 'app-point-list',
  templateUrl: './point-list.component.html',
  providers: [APIClient, PointSvc],
})
export class PointListComponent {
  points: Observable<any>;

  constructor(creds: PointSvc) {
    this.points = creds.getPoints();
  }
}
