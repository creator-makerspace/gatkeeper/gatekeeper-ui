import {Component} from '@angular/core';

import {APIClient} from '../apiclient.service';
import {PointSvc} from '../points.service';

@Component({
  selector: 'app-points',
  templateUrl: './point.component.html',
  styleUrls: ['./point.component.css'],
  providers: [APIClient, PointSvc],
})
export class PointsComponent {
}
