import { NgModule, ApplicationRef, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http } from '@angular/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OAuthModule } from 'angular-oauth2-oidc';

import { routing } from './app.routing';

// App stuff
import { ApplicationComponent } from './app.component';
import { OpenIDComponent } from './auth/openid.component';

import { ConfigSvc } from './config.service';
import { APIClient } from './apiclient.service';
import { UserSvc } from './users.service';
import { PointSvc } from './points.service';
import { CredentialSvc } from './credentials.service';

import { SharedModule } from './shared/shared.module';
import { CredentialModule } from './credential/credential.module';
import { UserModule } from './user/user.module';
import { PointModule } from './point/point.module';

import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ErrorComponent } from './error.component';

import { LoginComponent } from './auth/login.component';
import { LogoutComponent } from './auth/logout.component';

export function cfgFactory(configSvc: ConfigSvc): Function {
  return () => configSvc.load();
}

@NgModule({
  imports: [
    // Ng2 core
    BrowserModule,
    HttpModule,
    OAuthModule.forRoot(),

    // Styling components
    NgbModule,

    SharedModule,
    CredentialModule,
    UserModule,
    PointModule,
    routing
  ],
  declarations: [
    ApplicationComponent,
    NavbarComponent,
    SidebarComponent,
    OpenIDComponent,
    HomeComponent,
    DashboardComponent,
    ErrorComponent,
    LoginComponent,
    LogoutComponent,
  ],
  providers: [
    ConfigSvc,
    {
      provide: APP_INITIALIZER,
      useFactory: cfgFactory,
      deps: [ConfigSvc],
      multi: true
    },
    APIClient,
    CredentialSvc,
    PointSvc,
    UserSvc,
  ],
  bootstrap: [ApplicationComponent]
})
export class AppModule {}
