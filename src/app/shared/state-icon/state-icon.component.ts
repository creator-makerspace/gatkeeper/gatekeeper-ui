import {Component, Input, OnChanges, OnInit} from '@angular/core';

@Component({
  selector: 'app-state-icon',
  template: `<i class="fa fa-{{ icon }}" [style.color]="color"></i>`
})
export class StateIconComponent implements OnChanges, OnInit {
  @Input() stateBool: boolean = null;

  icon: string = null;
  color: string = null;

  setIcon() {
    this.icon = this.stateBool ? 'check-square' : 'fa-times';
    this.color = this.stateBool ? 'green' : 'red';
  }

  ngOnInit() {
    this.setIcon();
  }

  ngOnChanges() {
    this.setIcon();
  }
}
