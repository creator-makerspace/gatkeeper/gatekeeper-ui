import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-action-cb-all',
  templateUrl: './action-checkbox-all.component.html'
})
export class ActionCheckboxAllComponent {
  @Input() selected: Array<any>;
  @Input() selectable: Array<any>;
  @Output() checkedChange = new EventEmitter();

  isReady() {
    if (this.selected === undefined || this.selectable === undefined) {
      return false;
    }
    return true;
  }

  isDisabled() {
    if (this.isReady()) {
      return this.selectable.length === 0;
    }
    return true;
  }

  isAllSelected() {
    if (this.isReady()) {
      return this.selected.length === this.selectable.length;
    }
    return false;
  }

  selectAll() {
    if (this.selected.length !== 0) {
      this.checkedChange.emit(true);
    } else {
      for (let item of this.selectable) {
        this.selected.push(item.id);
      }

      this.checkedChange.emit(false);
    }
  }
}


@Component({
  selector: 'app-action-cb',
  templateUrl: './action-checkbox.component.html'
})
export class ActionCheckboxComponent {
  @Input() selected: Array<any>;
  @Input() identifier: string;

  check() {
    let idx = this.selected.indexOf(this.identifier);

    if (idx === -1) {
      this.selected.push(this.identifier);
    } else {
      this.selected.splice(idx, 1);
    }
  }

  isReady() {
    return this.selected !== undefined ? true : false;
  }

  isChecked() {
    return this.isReady() ? this.selected.indexOf(this.identifier) > -1 : false;
  }
}
