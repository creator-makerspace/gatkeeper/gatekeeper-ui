import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { OAuthService } from 'angular-oauth2-oidc';

import { ConfigSvc } from './config.service';


@Injectable()
export class APIClient {
  headers: Object = {};

  constructor(private http: Http, private authSvc: OAuthService, private configSvc: ConfigSvc) {
    this.headers['Authorization'] = 'Bearer ' + this.authSvc.getIdToken();
  }

  getUrl(path: string): string {
    let url = this.configSvc.get('gatekeeper')['apiUrl'] + path;
    return url;
  }

  setHeaders(headers: Object) {
    for (let k in headers) {
      if (headers.hasOwnProperty(k)) {
        this.headers[k] = headers[k];
      }
    }
  }

  get(path: string, params?: Object) {
    let url = this.getUrl(path);

    let headers = this.getHeaders();
    let urlParams = new URLSearchParams();

    if (params !== null) {
      for (let k in params) {
        if (params.hasOwnProperty(k)) {
          urlParams.set(k, params[k]);
        }
      }
    }

    return this.http.get(url, { headers: headers, search: urlParams })
      .map(resp => resp.json());
  }

  post(path: string, data?: Object) {
    let url = this.getUrl(path);

    let json = JSON.stringify(data);
    let headers = this.getHeaders({ 'Content-Type': 'application/json' });

    return this.http.post(url, json, { headers: headers })
      .map(resp => resp.json());
  }

  delete(path: string) {
    let url = this.getUrl(path);

    return this.http.delete(url, { headers: this.getHeaders() });
  }

  private getHeaders(headers?: Object): Headers {
    let ngHeaders = new Headers();

    for (let k in this.headers) {
      if (this.headers.hasOwnProperty(k)) {
        ngHeaders.set(k, this.headers[k]);
      }
    }

    for (let k in headers) {
      if (headers.hasOwnProperty(k)) {
        ngHeaders.set(k, headers[k]);
      }
    }
    return ngHeaders;
  }
}
