import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';

import { ConfigSvc } from './config.service';

@Component({
  selector: 'app-gatekeeper',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class ApplicationComponent implements OnInit {
  constructor(private router: Router, private cfg: ConfigSvc, private oauthService: OAuthService) {}

  ngOnInit() {
    if (this.cfg.cfg === null) {
      this.router.navigate(['error'], { replaceUrl: true });
      return;
    }

    this.oauthService.issuer = this.cfg.cfg.oidc.url
    this.oauthService.redirectUri = window.location.origin + '/openid';
    this.oauthService.clientId = this.cfg.cfg.oidc.clientId;
    this.oauthService.scope = 'openid profile email';
    this.oauthService.setStorage(sessionStorage);
    this.oauthService.showDebugInformation = true;
    this.oauthService.loadDiscoveryDocument().then(() => {
    })
  }
}
