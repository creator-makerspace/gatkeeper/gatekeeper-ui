import {Component} from '@angular/core';
import {Observable} from 'rxjs';

import {APIClient} from '../apiclient.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  events: any;
  expandedEvents: Object = {};

  constructor(private api: APIClient) {
    this.refresh();
    Observable.interval(100000).subscribe(() => this.refresh());
  }

  refresh() {
    this.api.get('/events').map(res => res.events).subscribe(
      data => {
        this.events = data;
      }
    );
  }
}
