import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {UserSvc} from '../users.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
})
export class UserDetailComponent {
  user: any = {};

  constructor(userSvc: UserSvc, private route: ActivatedRoute) {
    this.route.params
      .mergeMap(
        i => userSvc.getUser(i["id"])
      ).subscribe(
        data => this.user = data
      );
  }
}
