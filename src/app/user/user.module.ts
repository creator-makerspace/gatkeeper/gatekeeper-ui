import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {UsersComponent} from './user.component';
import {UserCreateComponent} from './user-create.component';
import {UserDetailComponent} from './user-detail.component';
import {UserListComponent} from './user-list.component';

import {SharedModule} from '../shared/shared.module';

import {routing} from './user.routing';

@NgModule({
  declarations: [
    UsersComponent,
    UserCreateComponent,
    UserDetailComponent,
    UserListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    routing,
  ]
})
export class UserModule {
}
